<?php

namespace App\Repository;

use App\Entity\Moderate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Moderate|null find($id, $lockMode = null, $lockVersion = null)
 * @method Moderate|null findOneBy(array $criteria, array $orderBy = null)
 * @method Moderate[]    findAll()
 * @method Moderate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModerateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Moderate::class);
    }

    // /**
    //  * @return Moderate[] Returns an array of Moderate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Moderate
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
