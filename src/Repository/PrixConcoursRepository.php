<?php

namespace App\Repository;

use App\Entity\PrixConcours;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PrixConcours|null find($id, $lockMode = null, $lockVersion = null)
 * @method PrixConcours|null findOneBy(array $criteria, array $orderBy = null)
 * @method PrixConcours[]    findAll()
 * @method PrixConcours[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrixConcoursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PrixConcours::class);
    }

    // /**
    //  * @return PrixConcours[] Returns an array of PrixConcours objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PrixConcours
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
