<?php

namespace App\Entity;

use App\Repository\LivreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LivreRepository::class)
 */
class Livre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $isbn;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=PrixConcours::class, mappedBy="livre", orphanRemoval=true)
     */
    private $concours;

    /**
     * @ORM\OneToMany(targetEntity=Ecris::class, mappedBy="livre", orphanRemoval=true)
     */
    private $auteursInfo;

    public function __construct()
    {
        $this->concours = new ArrayCollection();
        $this->auteursInfo = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|PrixConcours[]
     */
    public function getConcours(): Collection
    {
        return $this->concours;
    }

    public function addConcour(PrixConcours $concour): self
    {
        if (!$this->concours->contains($concour)) {
            $this->concours[] = $concour;
            $concour->setLivre($this);
        }

        return $this;
    }

    public function removeConcour(PrixConcours $concour): self
    {
        if ($this->concours->removeElement($concour)) {
            // set the owning side to null (unless already changed)
            if ($concour->getLivre() === $this) {
                $concour->setLivre(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Ecris[]
     */
    public function getAuteursInfo(): Collection
    {
        return $this->auteursInfo;
    }

    public function addAuteursInfo(Ecris $auteursInfo): self
    {
        if (!$this->auteursInfo->contains($auteursInfo)) {
            $this->auteursInfo[] = $auteursInfo;
            $auteursInfo->setLivre($this);
        }

        return $this;
    }

    public function removeAuteursInfo(Ecris $auteursInfo): self
    {
        if ($this->auteursInfo->removeElement($auteursInfo)) {
            // set the owning side to null (unless already changed)
            if ($auteursInfo->getLivre() === $this) {
                $auteursInfo->setLivre(null);
            }
        }

        return $this;
    }
}
