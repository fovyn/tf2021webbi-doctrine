<?php

namespace App\Entity;

use App\Repository\AuteurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AuteurRepository::class)
 */
class Auteur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenoù;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pseudo;

    /**
     * @ORM\OneToMany(targetEntity=Ecris::class, mappedBy="auteur", orphanRemoval=true)
     */
    private $ecris;

    public function __construct()
    {
        $this->ecris = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenoù(): ?string
    {
        return $this->prenoù;
    }

    public function setPrenoù(string $prenoù): self
    {
        $this->prenoù = $prenoù;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(?string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * @return Collection|Ecris[]
     */
    public function getEcris(): Collection
    {
        return $this->ecris;
    }

    public function addEcri(Ecris $ecri): self
    {
        if (!$this->ecris->contains($ecri)) {
            $this->ecris[] = $ecri;
            $ecri->setAuteur($this);
        }

        return $this;
    }

    public function removeEcri(Ecris $ecri): self
    {
        if ($this->ecris->removeElement($ecri)) {
            // set the owning side to null (unless already changed)
            if ($ecri->getAuteur() === $this) {
                $ecri->setAuteur(null);
            }
        }

        return $this;
    }
}
