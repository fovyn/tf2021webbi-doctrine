<?php

namespace App\Entity;

use App\Repository\PrixConcoursRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PrixConcoursRepository::class)
 */
class PrixConcours
{

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity=Livre::class, inversedBy="concours")
     * @ORM\JoinColumn(nullable=false)
     */
    private $livre;
    
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     */
    private $concours = "";

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLivre(): ?Livre
    {
        return $this->livre;
    }

    public function setLivre(?Livre $livre): self
    {
        $this->livre = $livre;

        return $this;
    }

    public function getConcours(): ?string
    {
        return $this->concours;
    }
    public function setConcours(string $value) {
        return $this->concours .= $value;
    }
}
