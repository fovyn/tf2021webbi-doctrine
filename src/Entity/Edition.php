<?php

namespace App\Entity;

use App\Repository\EditionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EditionRepository::class)
 */
class Edition
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $ordre;

    /**
     * @ORM\Column(type="float")
     */
    private $prix;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbExemplaire;

    /**
     * @ORM\Column(type="date")
     */
    private $dateImpression;

    /**
     * @ORM\ManyToOne(targetEntity=Livre::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $livre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(int $ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getNbExemplaire(): ?int
    {
        return $this->nbExemplaire;
    }

    public function setNbExemplaire(int $nbExemplaire): self
    {
        $this->nbExemplaire = $nbExemplaire;

        return $this;
    }

    public function getDateImpression(): ?\DateTimeInterface
    {
        return $this->dateImpression;
    }

    public function setDateImpression(\DateTimeInterface $dateImpression): self
    {
        $this->dateImpression = $dateImpression;

        return $this;
    }

    public function getLivre(): ?Livre
    {
        return $this->livre;
    }

    public function setLivre(?Livre $livre): self
    {
        $this->livre = $livre;

        return $this;
    }
}
