<?php

namespace App\Controller;

use App\Entity\Message;
use App\Repository\MessageRepository;
use App\Repository\SubjectRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageController extends AbstractController
{
    #[Route('/message', name: 'message')]
    public function index(): Response
    {
        return $this->render('message/index.html.twig', [
            'controller_name' => 'MessageController',
        ]);
    }

    /**
     * @Route("/user/{userId}/messages", name="message_list") 
     */
    public function listAction($userId, MessageRepository $messageRepository) {
        $messages = $messageRepository->findAllBySender($userId);

        return $this->render('message/list.html.twig', ["msgs" => $messages]);
    }

    /**
     * @Route("/message/create", name="message_create")
     */
    public function createAction(UserRepository $userRepo, SubjectRepository $subjectRepository, EntityManagerInterface $em) {
        $message = new Message();
        $message->setTitle("Titre créer");
        $message->setContent("Contenu créer");
        $message->setSender($userRepo->findOneById(1));
        $message->setSubject($subjectRepository->findOneById(1));
        
        $em->persist($message);

        $em->flush();

        return $this->redirectToRoute("message_list", ["userId" => 1]);
    }
}
