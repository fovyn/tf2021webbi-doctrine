<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/hello/{name}", name="say_hello")
     */
    public function sayHelloAction(String $name): Response
    {
        return $this->render('default/say-hello.html.twig', [
            'name' => $name,
        ]);
    }
}
