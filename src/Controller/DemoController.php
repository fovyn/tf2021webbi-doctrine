<?php

namespace App\Controller;

use App\Model\DemoIndexModel;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/demo")
 */
class DemoController extends AbstractController
{
    /**
     * @Route("/index/{username}", name="demo")
     */
    public function index($username, UserRepository $userRepository): Response
    {

        $users = $userRepository->findAll();

        $model = new DemoIndexModel();
        $model->setUsername($username);
        $model->setItems(["Flavian", "Aurélien", "Mélanie", "Quentin", "Luc"]);

        return $this->render(
            'demo/index.html.twig',
            ['model' => $model, 'users' => $users]
        );
    }

    /**
     * @Route("/list", name="demo_list")
     */
    public function listAction(): Response
    {
        return $this->render('demo/list.html.twig', [
            "items" => ["Flavian", "Oui messir", "Encore du travail", "Bieeenn"]
        ]);
    }
}
