<?php

namespace App\Model;

class DemoIndexModel {
    private $username;
    private $items;

    /**
     * Undocumented function
     *
     * @param String $username
     * @return void
     */
    public function setUsername($username) {
        $this->username = $username;
    }
    public function getUsername() {
        return $this->username;
    }

    public function setItems($items) {
        $this->items = $items;
    }
    public function getItems() {
        return $this->items;
    }
}