<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211007080530 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE moderate (start_at DATETIME NOT NULL, user_id INT NOT NULL, subject_id INT NOT NULL, end_at DATE DEFAULT NULL, INDEX IDX_6F2D035BA76ED395 (user_id), INDEX IDX_6F2D035B23EDC87 (subject_id), PRIMARY KEY(user_id, subject_id, start_at)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE moderate ADD CONSTRAINT FK_6F2D035BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE moderate ADD CONSTRAINT FK_6F2D035B23EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id)');
        $this->addSql('ALTER TABLE subject ADD creator_id INT NOT NULL, ADD created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE subject ADD CONSTRAINT FK_FBCE3E7A61220EA6 FOREIGN KEY (creator_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_FBCE3E7A61220EA6 ON subject (creator_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE moderate');
        $this->addSql('ALTER TABLE subject DROP FOREIGN KEY FK_FBCE3E7A61220EA6');
        $this->addSql('DROP INDEX IDX_FBCE3E7A61220EA6 ON subject');
        $this->addSql('ALTER TABLE subject DROP creator_id, DROP created_at');
    }
}
