<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211007122438 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE prix_concours MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE prix_concours DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE prix_concours ADD concours VARCHAR(255) NOT NULL, DROP id');
        $this->addSql('ALTER TABLE prix_concours ADD PRIMARY KEY (livre_id, concours)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE prix_concours ADD id INT AUTO_INCREMENT NOT NULL, DROP concours, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
    }
}
