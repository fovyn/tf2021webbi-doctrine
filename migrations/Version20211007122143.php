<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211007122143 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE edition ADD livre_id INT NOT NULL');
        $this->addSql('ALTER TABLE edition ADD CONSTRAINT FK_A891181F37D925CB FOREIGN KEY (livre_id) REFERENCES livre (id)');
        $this->addSql('CREATE INDEX IDX_A891181F37D925CB ON edition (livre_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE edition DROP FOREIGN KEY FK_A891181F37D925CB');
        $this->addSql('DROP INDEX IDX_A891181F37D925CB ON edition');
        $this->addSql('ALTER TABLE edition DROP livre_id');
    }
}
