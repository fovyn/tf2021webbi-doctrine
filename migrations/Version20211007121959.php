<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211007121959 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE auteur (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenoù VARCHAR(255) NOT NULL, pseudo VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commande (id INT AUTO_INCREMENT NOT NULL, edition_id INT NOT NULL, libraire_id INT NOT NULL, qtt INT NOT NULL, INDEX IDX_6EEAA67D74281A5E (edition_id), INDEX IDX_6EEAA67D948B1419 (libraire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ecris (id INT AUTO_INCREMENT NOT NULL, livre_id INT NOT NULL, auteur_id INT NOT NULL, prc_vente DOUBLE PRECISION NOT NULL, INDEX IDX_94251B9B37D925CB (livre_id), INDEX IDX_94251B9B60BB6FE6 (auteur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE edition (id INT AUTO_INCREMENT NOT NULL, ordre INT NOT NULL, prix DOUBLE PRECISION NOT NULL, nb_exemplaire INT NOT NULL, date_impression DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE libraire (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE livre (id INT AUTO_INCREMENT NOT NULL, isbn VARCHAR(255) NOT NULL, titre VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prix_concours (id INT AUTO_INCREMENT NOT NULL, livre_id INT NOT NULL, INDEX IDX_349148E437D925CB (livre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE commande ADD CONSTRAINT FK_6EEAA67D74281A5E FOREIGN KEY (edition_id) REFERENCES edition (id)');
        $this->addSql('ALTER TABLE commande ADD CONSTRAINT FK_6EEAA67D948B1419 FOREIGN KEY (libraire_id) REFERENCES libraire (id)');
        $this->addSql('ALTER TABLE ecris ADD CONSTRAINT FK_94251B9B37D925CB FOREIGN KEY (livre_id) REFERENCES livre (id)');
        $this->addSql('ALTER TABLE ecris ADD CONSTRAINT FK_94251B9B60BB6FE6 FOREIGN KEY (auteur_id) REFERENCES auteur (id)');
        $this->addSql('ALTER TABLE prix_concours ADD CONSTRAINT FK_349148E437D925CB FOREIGN KEY (livre_id) REFERENCES livre (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ecris DROP FOREIGN KEY FK_94251B9B60BB6FE6');
        $this->addSql('ALTER TABLE commande DROP FOREIGN KEY FK_6EEAA67D74281A5E');
        $this->addSql('ALTER TABLE commande DROP FOREIGN KEY FK_6EEAA67D948B1419');
        $this->addSql('ALTER TABLE ecris DROP FOREIGN KEY FK_94251B9B37D925CB');
        $this->addSql('ALTER TABLE prix_concours DROP FOREIGN KEY FK_349148E437D925CB');
        $this->addSql('DROP TABLE auteur');
        $this->addSql('DROP TABLE commande');
        $this->addSql('DROP TABLE ecris');
        $this->addSql('DROP TABLE edition');
        $this->addSql('DROP TABLE libraire');
        $this->addSql('DROP TABLE livre');
        $this->addSql('DROP TABLE prix_concours');
    }
}
